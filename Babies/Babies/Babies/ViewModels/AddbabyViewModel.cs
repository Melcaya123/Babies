﻿namespace Babies.ViewModels
{
   
    using System;
    using System.Windows.Input;
    using System.Collections.Generic;
    using Common.Models;
    using Helpers;
    using GalaSoft.MvvmLight.Command;
    using Services;
    using Plugin.Media.Abstractions;
    using Plugin.Media;
    using Xamarin.Forms;


    public class AddbabyViewModel : BaseViewModel
    {
        #region Atributes

        private MediaFile file;

        private ImageSource imageSource;

        private bool isRunning;

        private bool isEnabled;

        private ApiService apiService;

        private DateTime maxDate;

        private DateTime minDate;

        private string selectedTypeBlood;

        #endregion

        #region Properties

        public List<string> BloodTypeItem { get; set; }

        public string BabyName { get; set; }

        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public DateTime BornIn { get; set; }

        public string BloodType { get; set; }

        public string Observations { get; set; }

        public bool WasCesarean { get; set; }

        public bool IsRunning

        {
            get { return this.isRunning; }
            set { this.SetValue(ref this.isRunning, value); }
        }

        public ImageSource ImageSource
        {
            get { return this.imageSource; }
            set { this.SetValue(ref this.imageSource, value); }
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { this.SetValue(ref this.isEnabled, value); }
        }

        public DateTime MaxDate
        {
            get { return this.maxDate; }
            set { this.SetValue(ref this.maxDate, value); }
        }

        public DateTime MinDate
        {
            get { return this.minDate; }
            set { this.SetValue(ref this.minDate, value); }
        }

        public string SelectedTypeBlood
        {
            get { return this.selectedTypeBlood; }
            set { this.SetValue(ref this.selectedTypeBlood, value); }
        }

        #endregion

        #region Constructors
        public AddbabyViewModel()
        {
            this.IsEnabled = true;
            this.apiService = new ApiService();
            this.ImageSource = "NoPhoto";
            this.MaxDate = DateTime.Now;
            this.MinDate = DateTime.Now.AddYears(-30);
            this.BloodTypeItem = BloodTypeItemFill();
        }
        #endregion

        #region Metods

        private List<string> BloodTypeItemFill()
        {
            var LitsBlood = new List<string>();
            LitsBlood.Add("A+");
            LitsBlood.Add("A-");
            LitsBlood.Add("B-");
            LitsBlood.Add("O+");
            LitsBlood.Add("O-");
            LitsBlood.Add("AB+");
            LitsBlood.Add("AB-");
            return LitsBlood;
        }
       
        #endregion

        #region Commands

        public ICommand ChangeImageCommand
        {
            get
            {
                return new RelayCommand(ChangeImage);
            }
        }

        private async void ChangeImage()
        {
            await CrossMedia.Current.Initialize();
            var source = await Application.Current.MainPage.DisplayActionSheet(
                Languages.ImageSource, 
                Languages.Cancel, 
                null, Languages.FromGallery, 
                Languages.NewPicture);

            if (source == Languages.Cancel)
            {
                this.file = null; return;
            }

            if (source == Languages.NewPicture)
            {
                this.file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions{
                    Directory = "Sample",
                    Name = "test.jpg",
                    PhotoSize = PhotoSize.Small,
                });
            }
            else
            {
                this.file = await CrossMedia.Current.PickPhotoAsync();
            }

            if (this.file != null)
            {
                this.ImageSource = ImageSource.FromStream(() =>
                { var stream = file.GetStream(); return stream; });
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                return new RelayCommand(Save);
            }  
        }

        private async void Save()
        {
            if (string.IsNullOrEmpty(this.BabyName))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,  
                    Languages.NameBabyError,
                    Languages.Accept);
                return;
            }

            if (string.IsNullOrEmpty(this.FatherName))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NameFatherError,
                    Languages.Accept);
                return;
            }

            if (string.IsNullOrEmpty(this.MotherName))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NameMotherError,
                    Languages.Accept);
                return;
            }

            this.IsRunning = true;
            this.IsEnabled = false;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                     Languages.Error, connection.Message,
                     Languages.Accept);
                return;
            }

            byte[] imageArray = null;
            if (this.file != null)
            {
                imageArray = FilesHelper.ReadFully(this.file.GetStream());
            }

         
            var baby = new Baby
            {
                BabyName = this.BabyName,
                FatherName = this.FatherName,
                MotherName = this.MotherName,
                BornIn = this.BornIn,
                BloodType = this.SelectedTypeBlood,
                Observations = this.Observations,
                WasCesarean = this.WasCesarean,
                ImageArray = imageArray,
            };


            var url = Application.Current.Resources["UrlAPI"].ToString();
            var prefix = Application.Current.Resources["UrlPrefix"].ToString();
            var controller = Application.Current.Resources["UrlProductsController"].ToString();
            var response = await this.apiService.Post(url, prefix, controller,baby);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error, 
                    response.Message, 
                    Languages.Accept);
                return;
            }

            var newBaby =(Baby)response.Result;
            var babiesviewModel = BabiesViewModel.GetInstance();
            babiesviewModel.MyBabies.Add(newBaby);
            babiesviewModel.RefreshList();

            this.IsRunning = false;
            this.IsEnabled = true;
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        #endregion
    }
}
