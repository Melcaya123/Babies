﻿namespace Babies.ViewModels
{
    using System.Linq;
    using System;
    using System.Windows.Input;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Babies.Helpers;
    using Common.Models;
    using GalaSoft.MvvmLight.Command;
    using Services;
   
    using Xamarin.Forms;

    public class BabiesViewModel : BaseViewModel
    {
        #region Atributes

        public String filter;

        public int MyProperty { get; set; }

        public List<Baby> MyBabies { get; set; }

        private ApiService apiService;

        private bool isRefreshing;

        private ObservableCollection<BabyItemViewModel> babies;
        #endregion

        #region Properties

        public String Filter
        {
            get { return this.filter; }
            set
            {
                this.filter = value;
                this.RefreshList();
            }
        }

    public ObservableCollection<BabyItemViewModel> Babies
        {
            get { return this.babies; }
            set { this.SetValue(ref this.babies, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { this.SetValue(ref this.isRefreshing, value); }
        }
        #endregion

        #region Contrusctors
        public BabiesViewModel()
        {
            instance = this;
            this.apiService = new ApiService();
            this.LoadBabies();
        }
        #endregion

        #region Singleton
        private static BabiesViewModel instance;

        public  static BabiesViewModel GetInstance()
        {
            if (instance == null)
            {
                return new BabiesViewModel();
            }
            return instance;
        }

        #endregion

        #region Methods
        private async void LoadBabies()
        {
            this.IsRefreshing = true;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                      Languages.Error, connection.Message,
                      Languages.Accept);
                return;
            }
            var url = Application.Current.Resources["UrlAPI"].ToString();
            var prefix = Application.Current.Resources["UrlPrefix"].ToString();
            var controller = Application.Current.Resources["UrlProductsController"].ToString();
            var response = await this.apiService.GetList<Baby>(url, prefix, controller);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error, 
                    response.Message, 
                    Languages.Accept);
                return;
            }

            this.MyBabies = (List<Baby>)response.Result;
            this.RefreshList();
           
            this.IsRefreshing = false;
        }

        public void RefreshList()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                var MyListBabyItemViewModel = this.MyBabies.Select(p => new BabyItemViewModel
                {
                    BabyName = p.BabyName,
                    FatherName = p.FatherName,
                    MotherName = p.MotherName,
                    BornIn = p.BornIn,
                    BloodType = p.BloodType,
                    Observations = p.Observations,
                    WasCesarean = p.WasCesarean,
                    ImagePath = p.ImagePath,
                    ImageArray = p.ImageArray,
                    BabyId = p.BabyId,
                });
                this.Babies = new ObservableCollection<BabyItemViewModel>(
                    MyListBabyItemViewModel.OrderBy(p => p.BabyName));
            }
            else
            {
                var MyListBabyItemViewModel = this.MyBabies.Select(p => new BabyItemViewModel
                {
                    BabyName = p.BabyName,
                    FatherName = p.FatherName,
                    MotherName = p.MotherName,
                    BornIn = p.BornIn,
                    BloodType = p.BloodType,
                    Observations = p.Observations,
                    WasCesarean = p.WasCesarean,
                    ImagePath = p.ImagePath,
                    ImageArray = p.ImageArray,
                    BabyId = p.BabyId,
                }).Where(p => p.BabyName.ToLower().Contains(this.Filter.ToLower())).ToList();
                this.Babies = new ObservableCollection<BabyItemViewModel>(
                    MyListBabyItemViewModel.OrderBy(p => p.BabyName));
            }
           
        }

        #endregion

        #region Commands

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(RefreshList);
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadBabies);
            }
        }
        #endregion

    }
}
