﻿namespace Babies.ViewModels
{
    using System.Linq;
    using System.Windows.Input;
    using Babies.Common.Models;
    using Babies.Helpers;
    using GalaSoft.MvvmLight.Command;
    using Services;
    using Views;
    using Xamarin.Forms;

    public class BabyItemViewModel : Baby
    {
        #region Atributes
        private ApiService apiService;

        #endregion

        #region Constructor

        public BabyItemViewModel()
        {
            this.apiService = new ApiService();
        }

        #endregion

        #region Commands

        public ICommand EditBabyCommand
        {
            get
            {
                return new RelayCommand(EditBaby);
            }

        }

        private async void EditBaby()
        {
            MainViewModel.GetInstance().EditBaby = new EditBabyViewModel(this);
            await Application.Current.MainPage.Navigation.PushAsync(new EditBabyPage());
        }

        public ICommand DeleteBabyCommand
        {
            get
            {
                return new RelayCommand(DeleteBaby);
            }
            
        }

        private async void DeleteBaby()
        {
            var answer = await Application.Current.MainPage.DisplayAlert(
                Languages.Confirm, 
                Languages.DeleteComfirmation, 
                Languages.Yes, 
                Languages.No);

            if (!answer)
            {
                return;
            }

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                      Languages.Error, connection.Message,
                      Languages.Accept);
                return;
            }

            var url = Application.Current.Resources["UrlAPI"].ToString();
            var prefix = Application.Current.Resources["UrlPrefix"].ToString();
            var controller = Application.Current.Resources["UrlProductsController"].ToString();
            var response = await this.apiService.Delete(url, prefix, controller, this.BabyId);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error, 
                    response.Message, 
                    Languages.Accept);
                return;
            }

            var babiesViewModel = BabiesViewModel.GetInstance();
            var deleteBaby = babiesViewModel.MyBabies.Where(p => p.BabyId == this.BabyId).FirstOrDefault();
            if (deleteBaby != null)
            {
                babiesViewModel.MyBabies.Remove(deleteBaby);
            }

            babiesViewModel.RefreshList();
        }
        #endregion
    }
}
