﻿namespace Babies.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using Babies.Helpers;
    using Babies.Services;
    using Common.Models;
    using GalaSoft.MvvmLight.Command;
    using Plugin.Media;
    using Plugin.Media.Abstractions;
    using Xamarin.Forms;


    public class EditBabyViewModel : BaseViewModel
    {
        #region Atributes

        private Baby baby;

        private MediaFile file;

        private ImageSource imageSource;

        private bool isRunning;

        private bool isEnabled;

        private ApiService apiService;

        private DateTime maxDate;

        private DateTime minDate;

        private string selectedTypeBlood;

        #endregion

        #region Propierties

        public List<string> BloodTypeItem { get; set; }

        public Baby Baby
        {
            get { return this.baby; }
            set { this.SetValue(ref this.baby, value); }
        }

        public bool IsRunning

        {
            get { return this.isRunning; }
            set { this.SetValue(ref this.isRunning, value); }
        }

        public ImageSource ImageSource
        {
            get { return this.imageSource; }
            set { this.SetValue(ref this.imageSource, value); }
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { this.SetValue(ref this.isEnabled, value); }
        }

        public DateTime MaxDate
        {
            get { return this.maxDate; }
            set { this.SetValue(ref this.maxDate, value); }
        }

        public DateTime MinDate
        {
            get { return this.minDate; }
            set { this.SetValue(ref this.minDate, value); }
        }

        public string SelectedTypeBlood
        {
            get { return this.selectedTypeBlood; }
            set { this.SetValue(ref this.selectedTypeBlood, value); }
        }


        #endregion

        #region Constructors

        public EditBabyViewModel(Baby baby)
        {
            this.IsEnabled = true;
            this.apiService = new ApiService();
            this.ImageSource = baby.ImageFullPath;
            this.MaxDate = DateTime.Now;
            this.MinDate = DateTime.Now.AddYears(-30);
            this.baby = baby;
            this.SelectedTypeBlood = this.baby.BloodType;
            this.BloodTypeItem = BloodTypeItemFill();
        }


        #endregion

        #region Metods

        private List<string> BloodTypeItemFill()
        {
            var LitsBlood = new List<string>();
            LitsBlood.Add("A+");
            LitsBlood.Add("A-");
            LitsBlood.Add("B-");
            LitsBlood.Add("O+");
            LitsBlood.Add("O-");
            LitsBlood.Add("AB+");
            LitsBlood.Add("AB-");
            return LitsBlood;
        }

        #endregion

        #region Commands

        public ICommand DeleteCommand
        {
            get
            {
                return new RelayCommand(Delete);
            }
        }

        private async void Delete()
        {
            var answer = await Application.Current.MainPage.DisplayAlert(
                Languages.Confirm,
                Languages.DeleteComfirmation,
                Languages.Yes,
                Languages.No);

            if (!answer)
            {
                return;
            }

            this.isRunning = true;
            this.IsEnabled = false;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.isRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                      Languages.Error, connection.Message,
                      Languages.Accept);
                return;
            }

            var url = Application.Current.Resources["UrlAPI"].ToString();
            var prefix = Application.Current.Resources["UrlPrefix"].ToString();
            var controller = Application.Current.Resources["UrlProductsController"].ToString();
            var response = await this.apiService.Delete(url, prefix, controller, this.Baby.BabyId);

            if (!response.IsSuccess)
            {
                this.isRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                return;
            }

            var babiesViewModel = BabiesViewModel.GetInstance();
            var deleteBaby = babiesViewModel.MyBabies.Where(p => p.BabyId == this.Baby.BabyId).FirstOrDefault();
            if (deleteBaby != null)
            {
                babiesViewModel.MyBabies.Remove(deleteBaby);
            }

            babiesViewModel.RefreshList();

            this.isRunning = false;
            this.IsEnabled = true;

            await Application.Current.MainPage.Navigation.PopAsync();


        }

        public ICommand ChangeImageCommand
        {
            get
            {
                return new RelayCommand(ChangeImage);
            }
        }

        private async void ChangeImage()
        {
            await CrossMedia.Current.Initialize();
            var source = await Application.Current.MainPage.DisplayActionSheet(
                Languages.ImageSource,
                Languages.Cancel,
                null, Languages.FromGallery,
                Languages.NewPicture);

            if (source == Languages.Cancel)
            {
                this.file = null; return;
            }

            if (source == Languages.NewPicture)
            {
                this.file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    Directory = "Sample",
                    Name = "test.jpg",
                    PhotoSize = PhotoSize.Small,
                });
            }
            else
            {
                this.file = await CrossMedia.Current.PickPhotoAsync();
            }

            if (this.file != null)
            {
                this.ImageSource = ImageSource.FromStream(() =>
                { var stream = file.GetStream(); return stream; });
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                return new RelayCommand(Save);
            }
        }

        private async void Save()
        {
            if (string.IsNullOrEmpty(this.baby.BabyName))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NameBabyError,
                    Languages.Accept);
                return;
            }

            if (string.IsNullOrEmpty(this.baby.FatherName))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NameFatherError,
                    Languages.Accept);
                return;
            }

            if (string.IsNullOrEmpty(this.baby.MotherName))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NameMotherError,
                    Languages.Accept);
                return;
            }

            this.IsRunning = true;
            this.IsEnabled = false;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                     Languages.Error, connection.Message,
                     Languages.Accept);
                return;
            }

            byte[] imageArray = null;
            if (this.file != null)
            {
                imageArray = FilesHelper.ReadFully(this.file.GetStream());
                this.Baby.ImageArray = imageArray; 
            }

            this.baby.BloodType = SelectedTypeBlood;


            var url = Application.Current.Resources["UrlAPI"].ToString();
            var prefix = Application.Current.Resources["UrlPrefix"].ToString();
            var controller = Application.Current.Resources["UrlProductsController"].ToString();
            var response = await this.apiService.Put(url, prefix, controller, this.baby, this.Baby.BabyId);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                return;
            }

            var newBaby = (Baby)response.Result;
            var babiesviewModel = BabiesViewModel.GetInstance();
            var oldbaby = babiesviewModel.MyBabies.Where(p => p.BabyId == this.baby.BabyId).FirstOrDefault();

            if (oldbaby != null)
            {
                babiesviewModel.MyBabies.Remove(oldbaby);
            }

            babiesviewModel.MyBabies.Add(newBaby);
            babiesviewModel.RefreshList();

            this.IsRunning = false;
            this.IsEnabled = true;
            await Application.Current.MainPage.Navigation.PopAsync();
        }
        #endregion
    }
}
