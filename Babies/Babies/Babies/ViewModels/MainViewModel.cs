﻿namespace Babies.ViewModels
{
    using System.Windows.Input;
    using Xamarin.Forms;
    using GalaSoft.MvvmLight.Command;
    using Views;

    public class MainViewModel
    {
        #region Propierties

        
        public EditBabyViewModel EditBaby { get; set; }

        public BabiesViewModel Babies { get; set; }

        public AddbabyViewModel AddBaby { get; set; }

        #endregion

        #region Constructors

        public MainViewModel()
        {
            instance = this;
            this.Babies = new BabiesViewModel();
        }

        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            return instance;
        }

        #endregion

        #region Commands
        public ICommand AddBabyCommand
        {
            get
            {
                return new RelayCommand(GoToAddBaby);
            }

        }

        private async void GoToAddBaby()
        {
            this.AddBaby = new AddbabyViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new AddBabyPage());
        }

        #endregion
    }
}
